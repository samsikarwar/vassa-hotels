<?php
// Replace 'path/to/your/pdf/file.pdf' with the actual path to your PDF file
$pdf_file = 'pdf/VASSA_WELLNESS CLUB - Highlight .pdf';

// Set the headers to force a download dialog box
header('Content-Type: application/octet-stream');
header('Content-Type: application/download');
header('Content-Disposition: attachment; filename="' . basename($pdf_file) . '"');
header("Content-Description: File Transfer");
header('Content-Length: ' . filesize($pdf_file));

// Send the file contents to the browser
readfile($pdf_file);
?>