<?php
?>

<?php

?>

<!DOCTYPE html>
<!-- saved from url=(0035)https://stunningcollection.com/vasa -->
<html class="js-focus-visible" data-js-focus-visible=""><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="format-detection" content="telephone=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<title></title>
	<link rel="stylesheet" href="./vasa_files/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<link rel="stylesheet" href="./vasa_files/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	<link href="./vasa_files/font-awesome.min.css" rel="stylesheet">
	
	<link type="text/css" href="./vasa_files/vasa.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Damion&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Scheherazade+New:wght@400;700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">
<style type="text/css">
@font-face {
  font-weight: 400;
  font-style:  normal;
  font-family: 'Circular-Loom';

  src: url('https://cdn.loom.com/assets/fonts/circular/CircularXXWeb-Book-cd7d2bcec649b1243839a15d5eb8f0a3.woff2') format('woff2');
}

@font-face {
  font-weight: 500;
  font-style:  normal;
  font-family: 'Circular-Loom';

  src: url('https://cdn.loom.com/assets/fonts/circular/CircularXXWeb-Medium-d74eac43c78bd5852478998ce63dceb3.woff2') format('woff2');
}

@font-face {
  font-weight: 700;
  font-style:  normal;
  font-family: 'Circular-Loom';

  src: url('https://cdn.loom.com/assets/fonts/circular/CircularXXWeb-Bold-83b8ceaf77f49c7cffa44107561909e4.woff2') format('woff2');
}

@font-face {
  font-weight: 900;
  font-style:  normal;
  font-family: 'Circular-Loom';

  src: url('https://cdn.loom.com/assets/fonts/circular/CircularXXWeb-Black-bf067ecb8aa777ceb6df7d72226febca.woff2') format('woff2');
}


h1, h2, h3, h4, h5, h6{
    font-family: 'Scheherazade New';
    color:#fff;
}


p{
    font-family: 'Open Sans', sans-serif;
     color:#fff;
}
.custom-iframe-overlay {
    position: absolute;
    top: 20%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 100%;
    max-width: 1440px;
}
.custom-iframe-overlay .overlay-text>h1 {
    font-size: 50px;
    font-family: damion;
}
.custom-iframe-overlay h1 {
    color: #fff;
}
.custom-iframe-overlay h1 a {
    color: #fff;
}
.custom-iframe-overlay h1 a:hover {
    color: #fff;
    text-decoration:none;
}
#hotels .img-hed1 {
    position: relative;
    top: unset;
    left: unset;
}
.custom-iframe-overlay .f9banner-content h1 {
    font-size: 21px;
    padding: 0 15px;
}

.button-grp a {
    height: auto;
    width: auto;
}
.gallery_outer.container h1,
.gallery_outer.container p{
    text-align: center;
}
.gallery_outer .img-b-s {
    TEXT-ALIGN: center;
}
.gallery_outer .img-b-s .img-fluid {
    WIDTH: 100%;
    MAX-WIDTH: 500PX;
}

.gallery_outer .hotel-grp {
    justify-content: center;
}

.gallery_outer.container .col-lg-12>p {
    width: 70%;
    margin: auto;
    margin-bottom: 65px;
}

.gallery_outer .hotel-grp h1 {
    padding-top: 25px;
}

.gallery_outer .hotel-grp .button-grp .btn,
.about .about-sec .btn,
.discover-explore .button-grp .btn{
    font-size: 15px;
    border: 1px solid #fff;
    border-radius: 0;
    box-shadow: 0 0 3px -1px rgba(37,40,45,.7);
    background-color: #000;
    color: #fff;
    font-family: Open Sans;
    font-weight: 600;
    letter-spacing: 0;
    line-height: 27px;
    text-align: center;
    min-width: 200px;
    min-height: 48px;
    display: inline-flex;
    justify-content: center;
    align-items: center;
        margin-right: 20px;
}
.gallery_outer .hotel-grp .button-grp .btn.black-btn {
    background-color: #fff;
    color: #000;
}

.gallery_outer .hotel-grp .button-grp,
.about .about-sec .button-grp,
.discover-explore .button-grp{
    display: flex;
    justify-content: center;
    margin-top: 25px;
}

.about .container .col-lg-12>p {
    width: 70%;
    margin: auto;
    margin-bottom: 65px;
}

.about .container h1, 
.about .container p,
.discover-explore .container h1, 
.discover-explore .container p{
    text-align: center;
}

.about .about-sec {
    align-items: center;
}
.about .img-b-s .img-fluid {
    width: 100%;
    height: 600px;
    object-fit: cover;
        object-position: 100% 0%;
}
.about {
    padding-top: 100px;
}
.about .about-sec .col-lg-6 {
    padding: 0;
}
.about .about-sec p {
    width: 75%;
    text-align: center;
    margin: AUTO;
}


.discover-explore p{
    width: 75%;
    text-align: center;
    margin: AUTO;
}

.marker-line {
    height: 2px;
    background: rgb(202 202 202 / 48%);
    width: 50%;
    margin: auto;
    margin-top: 100px;
    margin-bottom: 100px;
}

.discover-explore .discover-sec .img-b-s .img-fluid {
    width: 100%;
    height: 450px;
    object-fit: cover;
    object-position: 100% 10%;
}


.discover-explore .discover-sec {
    align-items: center;
}

.comming-soon .container .row {
    justify-content: center;
}


.scssc_outer {
    padding: 20px 0;
}

.gallery_outer .hotel-grp .col-lg-5 p {
    min-height: 263px;
}
.lets_talk_outer .va-min {
    font-size: 70px;
    font-style: normal!important;
}
.signature_btm p {
    font-weight: 300;
}
#partner .offer_cnt {
    text-align: center;
}
#partner .offer_cnt img {
    max-width:250px;
}
.csst-footer-grp .scssc_cnt p {
    margin-bottom: 8px;
}
.csst-footer-grp .row:nth-of-type(2) {
    margin-top: 0;
}
.gallery_outer .img-b-s .img-fluid {
    WIDTH: 100%;
    MAX-WIDTH: 500PX;
    height: 500px;
    object-fit: cover;
    object-position: top;
}
.contact_outer {
    padding: 50px 0;
}
.csst-footer-grp {
    border-top: 1px solid #80808073;
}
.cst-btn{
    background: #fff;
    width: 150px;
    height: 60px;
    padding: 0;
    text-align: center;
    line-height: 60px;
    font-size: 18px;
    font-weight: bold;
    text-transform: uppercase;
    letter-spacing: 3px;
    color: #000;
    cursor: pointer;
    text-decoration: none;
    vertical-align: middle;
    border-radius: 5px;
}
.cst-btn a {
    color: #000;
}
.csst-footer-grp p {
    font-size: 14px;
}
.csst-footer-grp .insta-logo {
    max-width: 25px;
    margin-bottom: 15px;
    margin-right: 25px;
}
.more_about_membership {
    width: 100%;
    display: inline-block;
    padding: 70px 0;
    text-align: center;
    background-color: #32323257;
    padding-bottom: 75px;
}
.more_about_membership h2 {
    font-family: 'proxima_novalight';
    font-size: 37px;
    margin: 0 0 20px;
}
.more_about_membership p {
    font-size: 16px;
    margin: 0 0 20px;
}
.travel_contact {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    max-width: 700px;
    overflow: hidden;
    margin: 20px auto 0;
    background-color: #fff;
    box-shadow: 0 2px 8px rgba(0,0,0,.15);
}
.left_travel_ing {
    width: 55%;
    float: left;
}
.right_travel_contact {
    float: right;
    width: 45%;
    text-align: left;
    -webkit-box-align: center !important;
    -ms-flex-align: center !important;
    align-items: center !important;
    display: -webkit-box !important;
    display: -ms-flexbox !important;
    display: flex !important;
}
.left_travel_ing img {
    width: 100%;
    height: 280px;
    object-fit: cover;
    object-position: top;
}
.travel_coninner {
    float: left;
    width: 100%;
    padding: 0 20px;
}
.travel_coninner h3 {
    font-size: 24px;
    font-family: 'proxima_novasemibold';
    margin: 0 0 10px;
    color:#000;
}
.travel_coninner ul {
    padding: 0;
    float: left;
    width: 100%;
    margin: 0;
}
.travel_coninner ul li {
    display: inline-block;
    width: 100%;
    margin: 0;
    padding: 0 0;
    border-top: 1px solid #d7d7d7;
}
.travel_coninner ul li a {
    text-decoration: none;
    font-size: 18px;
    padding: 12px 0;
    display: inline-block;
        color: #1e1e1e;
}
.travel_coninner ul li a p{
    color: #1e1e1e;
    font-size: 15px;
    margin-bottom:0;
}

.footer_become {
    width: 100%;
    padding: 20px 0;
}
.footer_become h1 {
    margin: 0px 0 5px;
    font-size: 29px;
    color: #fff;
}
.footer_become p {
    margin: 0 0 10px;
    line-height: normal;
    font-size: 18px;
    padding-right: 30px;
}
.submit_request {
    padding: 14px 20px;
    background-color: #fff;
    color: #000;
    font-size: 19px;
    margin: 0 auto;
    text-decoration: none;
    transition: 0.5s;
}
.submit_request:hover {
    color: #000;
    text-decoration: none;
}
.travel_coninner ul li a.add-flex {
    DISPLAY: FLEX;
}

.become-a-partner.modal.fade .modal-dialog{
    top: 50%;
    Left:50%;
    transition:none;
}


.become-a-partner.modal.show .modal-dialog {
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
    margin: 0;
}
.close.model_close {
    position: absolute;
    right: 0;
    width: 40px;
    height: 40px;
    cursor: pointer;
    z-index: 99;
    background-color: transparent;
    opacity: 1;
    color: #252748;
    border-radius: 100px;
    top: 0;
    right: 1px;
    font-weight: normal;
    font-size: 21px;
}

.coomn_form form input {
    padding: 10px 12px;
    border: 1px solid #d7d7d7;
    width: 100%;
    color: #333;
    height: 48px;
    font-size: 14px;
}
.coomn_form form .form-group {
    position: relative;
}
.custom_checkbox {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 0;
    cursor: pointer;
    font-size: 14px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    height: 25px;
    line-height: normal;
}
.coomn_form label.custom_checkbox input {
    position: absolute;
    width: 26px;
    height: 26px;
    left: 0;
    top: 0;
}
.custom_checkbox .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: transparent;
    border: 1px solid #d7d7d7;
}
.coomn_form form input.page_btn {
    height: 50px;
    padding: 0;
    margin: 0px 0 0;
    background-color: #000;
    color: #fff;
    border: 2px solid #bbced9;
    font-family: 'proxima_novasemibold';
    cursor: pointer;
}
.custom_checkbox input:checked ~ .checkmark {
    background-color: #bbced9;
    border-color: #bbced9;
}
.custom_checkbox .checkmark:after {
    content: "";
    position: absolute;
    display: none;
}
.custom_checkbox .checkmark::after {
    left: 9px;
    top: 5px;
    width: 6px;
    height: 10px;
    border: solid white;
    border-width: 0 2px 2px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
.custom_checkbox input:checked ~ .checkmark:after {
    display: block;
}
div#become_amember {
    z-index: 99999;
}
@media (max-width: 1200px){
    .gallery_outer .hotel-grp {
    justify-content: space-around;
}
.menu_items ul li a {
    padding: 10px 6px;
    font-size: 13px;
}
.gallery_outer .hotel-grp .col-lg-5 p {
    min-height: 354px;
}
}





@media (max-width: 990px){
   .gallery_outer.container .col-lg-12>p {
    width: 100%;
} 
.gallery_outer .hotel-grp .col-lg-5 {
    margin-bottom: 45px;
}
.about .about-sec2 .about-sec2-img {
    order: 2;
}
.about .about-sec2 {
    margin-top: 50px;
    margin-bottom: 50px;
}
.gallery_outer .hotel-grp .button-grp, 
.about .about-sec .button-grp, 
.discover-explore .button-grp {
    margin-bottom: 40px;
}
.about {
    padding-top: 10px;
}
.marker-line {
    margin-top: 80px;
    margin-bottom: 80px;
}
.discover-explore .discover-sec.discover-sec2 .discover-sec-i2 {
    order: 2;
}
.eyebrow-dark {
    margin-top: 25px;
}
.lets_talk_outer .va-min {
    font-size: 70px;
    font-style: normal!important;
}
.toggle_btn {
    display: block;
}
.menu_items > ul {
    transition: all ease-in-out 0.3s;
    position: absolute;
    width: 0%;
    overflow: hidden;
    height: 100vh;
    background: #000;
    right: 0;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
}
.gallery_outer .hotel-grp .col-lg-5 p {
    min-height: auto; 
    padding-bottom: 20px;
}
}






@media (max-width: 768px){
.custom-iframe-overlay .overlay-text>h1 {
    font-size: 35px;
}
.gallery_outer .hotel-grp .button-grp .btn, .about .about-sec .btn, .discover-explore .button-grp .btn{
   min-width: 165px;
}

.gallery_outer .hotel-grp .button-grp .btn:nth-of-type(2),
.about .about-sec .btn:nth-of-type(2), 
.discover-explore .button-grp .btn:nth-of-type(2){
    margin: 0;
}
.scssc_outer {
    padding: 0px;
}
.more_about_membership {
    padding: 40px 15px;
}
.more_about_membership h2 {
    font-size: 26px;
    margin: 0 0 5px;
}.more_about_membership p {
    font-size: 14px;
    margin: 0 0 8px;
}
.travel_contact {
    flex-flow: row wrap;
    padding: 0 0;
}
.left_travel_ing {
    width: 100%;
}
.right_travel_contact {
    width: 100%;
    padding-top: 16px;
}
.travel_coninner h3 {
    font-size: 19px;
}
.travel_coninner ul li a {
    font-size: 15px;
    padding: 9px 0px;
}
}



@media (max-width: 480px){

h1{
    font-size:30px;
}    
    
.custom-iframe-overlay .overlay-text>h1 {
    font-size: 22px;
}
.custom-iframe-overlay {
    top: 42%;
}
.marker-line {
    margin-top: 50px;
    margin-bottom: 50px;
}
.discover-explore p {
    width: 95%;
}

.about .container .col-lg-12>p {
    width: 90%;
}
.gallery_outer .hotel-grp .col-lg-5 {
    margin-bottom: 25px;
}
.about .about-sec p {
    width: 85%;
    margin-bottom: 45px;
}
.discover-explore .discover-sec .col-lg-6,
.about .about-sec .col-lg-6{
    margin-top: 15px;
    margin-bottom: 15px;
}
.gal_outer {
    padding: 0;
}
.csst-footer-grp h1 {
    text-align: left !important;
        font-size: 30px!important;
}
.discover-explore h1,
.gallery_outer.container h1{
    padding-top: 40px;
}
.csst-footer-grp p {
    font-size: 12px;
    text-align: left!important;
}
.csst-footer-grp .insta-logo {
    margin-bottom:15px;
}
.footer_become h1 {
    margin: 0;
    text-align: center;
    font-size: 22px;
}
.footer_become p {
    text-align: center;
    font-size: 12px;
    padding-right: 0;
}
.submit_request {
    font-size: 14px;
    padding: 8px 10px;
}
.more_about_membership {
    padding: 0px 15px;
}
.travel_coninner ul li a {
    font-size: 13px;
}
.footer_become {
    padding-bottom: 30px;
}
.csst-footer-grp .row:nth-of-type(2) {
    margin-top: 0;
}
.travel_coninner ul li a p{
     color: #1e1e1e;
         font-size: 13px;
}
}




@media (max-width: 375px){
.gallery_outer .hotel-grp .button-grp .btn, .about .about-sec .btn, .discover-explore .button-grp .btn {
    min-width: 120px;
    font-size: 12px;
    line-height: 1.1;
}
.csst-footer-grp h1 {
    font-size: 26px!important;
}
}





</style>

</head>
<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="">
	<header class="header_outer">
		<div class="container">
			<nav>
				<div class="logo_main">
					<a href="https://vassahotels.com"><img src="./vasa_files/Vasas_logo.png"></a>
				</div>
				<div class="menu_items">
					<button class="toggle_btn"><span></span></button>
					<ul>
						<!--<li><a href="https://demo.vassahotels.com/">Home</a></li>-->
						<li><a href="#about">About Us</a></li>
						<li>
							<a href="#hotels" class="drp_btn">Hotels</a>
							<!--<ul class="drop_outer">-->
							<!--	<li><a href="#hotel">Vassa Hotels</a></li>-->
							<!--	<li><a href="#collection">The Stunning Collection</a></li>-->
							<!--	<li><a href="#Life">Vassa Life</a></li>-->
							<!--</ul>-->
						</li>
						<li><a href="#wellness">Wellness Clinic</a></li>
						<li><a href="#member">Member's Club</a></li>
						<li><a href="#culinary">Culinary</a></li>
						<li><a href="#art">Art & Fashion</a></li>
						<li><a href="#event">Events</a></li>
						<li><a href="#career">Career</a></li>
						<li><a href="#partner">Partners</a></li>
						<li><a href="#contact">Contact Us</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</header>
	<section class="hero2 mb-2">
  		<div class="vimeo-wrapper">
        <iframe src="https://player.vimeo.com/video/809101757?background=1&amp;autoplay=1&amp;loop=1&amp;byline=0&amp;title=0"  frameborder="0" webkitallowfullscreen=""  mozallowfullscreen="" allowfullscreen="">
            </iframe>
    	</div>
    	 <!--<div class="custom-iframe-overlay">-->
      <!--      <div class="overlay-text">-->
      <!--          <h1>Luxury meets authenticity at Vassa Hospitality.</h1>-->
      <!--      </div>-->
                           
      <!--      <div class="f9banner-content">-->
      <!--        <h1><a href="" class="typewrite" data-period="2000" data-type="[ &quot;Vassa Hotels: Experience luxury like no other.&quot;, &quot;Vassa Wellness Clinics: Enhance your natural beauty and improve your overall wellbeing.&quot;, &quot;Vassa Member's Club: Exclusive experiences for discerning individuals.&quot;, &quot;Partners: The ultimate in luxury and exclusivity.&quot;, &quot;Founders: Elena Vassa - A vision of simplicity and excellence.&quot; ]"><span class="wrap"></span></a></h1>-->
      <!--      </div>-->
      <!--      </div>-->
	</section>
	<section class="gal_outer" id="about">
	    <div class="gallery_outer container">
	        <div class="row">
	            <div class="col-lg-12">
	                <h1>VASSA</h1>
	                <p>Welcome to Vassa, where bespoke luxury and wellness converge to elevate your life's story. I truly believe luxury is doing simple things right in a non-complicated way and service has to be genuine from the heart-not a branded approach. Our hotels offer an unrivaled experience, where you can indulge in bespoke spa treatments, savor exquisite cuisine, and unwind in luxurious accommodations. Our wellness clinic is staffed with experienced practitioners who can help you achieve optimal health and well-being through tailored programs. And as a member of Vassa's exclusive club, you'll enjoy access to VIP services, events, and amenities that enhance your lifestyle. Discover the Vassa difference today and elevate your story to new heights.</p>
	            </div>
	        </div>
            <div class="row hotel-grp"  id="hotels">
                <div class="col-lg-5">
                    <div class="img-b-s">
                      <img src="./vasa_files/vassa-1.jpg" class="img-fluid">
                    </div>
                    <h1>VASSA HOTELS</h1>
                    <p>Our hotels are destinations that inspire more than just a place to stay. Each property is thoughtfully designed to reflect the local culture and offer bespoke experiences that cater to your every need. We specialize in crafting personalized experiences that reflect the local culture and provide guests with a true sense of place. From personalized service to luxurious amenities, we ensure that your stay is nothing less than extraordinary. VASSA’s robust booking/sales, distribution and operation management infrastructure to improve the productivity and profitability while enhancing the guest's experience. A WORLD OF INFINITE EXPERIENCE</p>
                    <div class="button-grp">
                         <a class="btn trans-btn" href="pdf/hotels.pdf" target="_blank">VIEW</a>
                        <a class="btn black-btn" href="pdf/hotels.pdf" download target="_blank">DOWNLOAD</a>
                    </div>
                </div>
                <div class="col-lg-5" id="wellness">
                     <div class="img-b-s">
                      <img src="./vasa_files/vassa-2.jpg" class="img-fluid">
                    </div>
                    <h1>VASSA WELLNESS CLINIC</h1>
                    <p>At Vassa, we believe that wellness is not just a trend, but a lifestyle. Our clinic offers a comprehensive range of services, from traditional therapies to cutting-edge treatments, all delivered by expert practitioners in a serene setting. Our personalized approach ensures that every aspect of your well-being is taken care of, so you can leave feeling rejuvenated and renewed. Enhance your life's story with our wellness services. VASSA hotel’s team of international luxury hoteliers, we will empower small operators, hotel owners, and travellers' worldwide to rethink and streamline the entire hospitality infrastructure.</p>
                    <div class="button-grp">
                       
                        <a class="btn trans-btn" href="pdf/vassa.pdf" target="_blank">VIEW</a>
                        <a class="btn black-btn" href="pdf/vassa.pdf" download target="_blank">DOWNLOAD</a>
                    </div>
                </div>
            </div>
        </div>
	</section>
	<section id="about" class="about">
		<div class="container">
        			<div class="row about-sec" id="member">
                        <div class="col-lg-6">
                            <!--<h6 class="eyebrow-dark" style="text-align: center;">STAY</h6>-->
                            <h1>MEMBER CLUB</h1>
        	                <p>Elevate your lifestyle with Vassa's exclusive club membership. As a member, you'll gain access to a world of luxury and exclusivity, from private events and VIP experiences to personalized services and amenities that cater to your every need. Let us enhance your life's story and create unforgettable moments that you'll treasure forever. Join Vassa's exclusive member club today.</p>
                            <!--<div class="button-grp">-->
                            <!--    <a class="btn trans-btn" href="#">DISCOVER MORE</a>-->
                            <!--    <a class="btn" href="#">BOOK NOW</a>-->
                            <!--</div>-->
                        </div>
                        <div class="col-lg-6">
                             <div class="img-b-s">
                              <img src="./vasa_files/vassa-3.jpg" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="row about-sec about-sec2" id="culinary"> 
                        <div class="col-lg-6 about-sec2-img">
                             <div class="img-b-s">
                              <img src="./vasa_files/vassa-4.jpg" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <!--<h6 class="eyebrow-dark" style="text-align: center;">STAY</h6>-->
                            <h1>CULINARY</h1>
                            <p>Our culinary experiences are inspired by cultures from around the world, with a focus on fresh, local ingredients. We partner with renowned chefs and restaurants to offer our guests an unforgettable culinary experience. Whether it's a gourmet meal or a cooking class, our culinary experiences are tailored to meet your preferences and ensure an exceptional dining experience. Explore Vassa's culinary offerings today.</p>
                            <!--<div class="button-grp">-->
                            <!--    <a class="btn trans-btn" href="#">DISCOVER MORE</a>-->
                            <!--    <a class="btn" href="#">BOOK NOW</a>-->
                            <!--</div>-->
                        </div>
                    </div>
                    <div class="row about-sec" id="art">
                        <div class="col-lg-6">
                            <!--<h6 class="eyebrow-dark" style="text-align: center;">STAY</h6>-->
                            <h1>ART & FASHION</h1>
                            <p>Vassa Hospitality is not just about providing luxurious experiences but also embracing art and fashion. We partner with leading fashion designers, promoting exclusive collections and organizing fashion events. Our cultural-inspired art collections are showcased throughout our properties, providing our guests with a unique and immersive experience. Immerse yourself in Vassa's art and fashion offerings.</p>
                            <!--<div class="button-grp">-->
                            <!--    <a class="btn trans-btn" href="#">DISCOVER MORE</a>-->
                            <!--    <a class="btn" href="#">BOOK NOW</a>-->
                            <!--</div>-->
                        </div>
                        <div class="col-lg-6">
                             <div class="img-b-s">
                              <img src="./vasa_files/vassa-5.jpg" class="img-fluid">
                            </div>
                        </div>
                    </div>
	    </div>
</section>
<section class="chessboard-marker">
    <div class="container">
        <div class="marker-line"></div>
    </div>
</section>
<section id="event" class=" discover-explore">
  		<div class="container">
    		<div class="row discover-sec">
    		        <div class="col-lg-6">
                             <div class="img-b-s">
                              <img src="./vasa_files/vassa-6.jpg" class="img-fluid">
                            </div>
                    </div>
                    <div class="col-lg-6">
                            <!--<h6 class="eyebrow-dark" style="text-align: center;">STAY</h6>-->
                            <h1>EVENTS</h1>
                            <p>Our brand philosophy is to provide exceptional and personalized experiences for our guests, and our events are no exception. We offer a range of food events, cultural events, and wellness events, all tailored to meet our guests' preferences. Our events are designed to immerse our guests in the local culture, offering an unforgettable and authentic experience. Discover Vassa's events today.</p>
                            <!--<div class="button-grp">-->
                            <!--    <a class="btn trans-btn" href="#">DISCOVER MORE</a>-->
                            <!--    <a class="btn" href="#">BOOK NOW</a>-->
                            <!--</div>-->
                    </div>
                </div>
    		</div>
        </div>
	</section>
<section class="chessboard-marker">
    <div class="container">
        <div class="marker-line"></div>
    </div>
</section>
<section id="career" class=" discover-explore">
  		<div class="container">
    		<div class="row discover-sec discover-sec2">
                    <div class="col-lg-6 discover-sec-i2">
                            <!--<h6 class="eyebrow-dark" style="text-align: center;">STAY</h6>-->
                            <h1>CAREER</h1>
                            <p>At Vassa Hospitality, we believe in investing in our ambassadors' growth and development, providing them with opportunities to excel and achieve their goals. We are always looking for talented individuals who share our passion for excellence and attention to detail. Join our team and become part of a brand that values and rewards dedication and hard work. Explore Vassa's career opportunities today.</p>
                            <!--<div class="button-grp">-->
                            <!--   <a class="btn trans-btn" href="#">DISCOVER MORE</a>-->
                            <!--    <a class="btn" href="#">BOOK NOW</a>-->
                            <!--</div>-->
                    </div>
                    <div class="col-lg-6 ">
                             <div class="img-b-s">
                              <img src="./vasa_files/vassa-7.jpg" class="img-fluid">
                              
                            </div>
                    </div>
                </div>
    		</div>
        </div>
</section>
	
	
	<!--<section id="wellness">-->
	<!--	<div class="container">-->
	<!--		<div class="row frist mt-2" id="five">-->
	<!--		<div class="col-md-8">-->
	<!--			<div class="main-top mt-4">-->
	<!--				<h1>Wellness Clinic </h1>-->
	<!--			</div>-->
	<!--		</div>-->
	<!--	</div>-->
	<!--	<div class="mt-2">-->
	<!--			<div class="row frist">-->
	<!--				<div class="col-md-7">-->
	<!--					<div class="min-top-sec">-->
	<!--						<p>-->
	<!--						At Vassa, we believe that wellness is not just a trend, but a lifestyle.-->
	<!--					</p>-->
	<!--					<p>-->
	<!--						Our clinic offers a comprehensive range of services, from traditional therapies to cutting-edge treatments, all delivered by expert practitioners in a serene setting. Our personalized approach ensures that every aspect of your well-being is taken care of, so you can leave feeling rejuvenated and renewed. Enhance your life's story with our wellness services.-->
	<!--					</p>-->
						<!--<p>-->
						<!--	VASSA hotel’s team of international luxury hoteliers, we will empower small operators, hotel owners, and travellers' worldwide to rethink and streamline the entire hospitality infrastructure.-->
						<!--</p>-->
						<!--<p>-->
						<!--	VASSA Loyalty is rewarded with TravolierGOLD award-winning loyalty program, where member’s earn Gold for loyalty – with the power to exchange Gold points with over 500K hotels &amp; 700 airlines.-->
						<!--</p>-->
						<!--<p>-->
						<!--	VASSA’s robust booking/sales, distribution and operation management infrastructure to improve the productivity and profitability while enhancing the guest's experience.-->
						<!--</p>-->
						<!--<p class="last-p">-->
						<!--	A WORLD OF INFINITE EXPERIENCE-->
						<!--</p>-->
	<!--					</div>-->
	<!--				</div>-->
	<!--				<div class="col-md-5">-->
	<!--					<div class="d-flex flex-row flex-wrap justify-content-center ele-sec">-->
	<!--              			<div class="d-flex flex-column">-->
	<!--                  			<img src="./vasa_files/48_JXDOiBc.jpg.768x1024_q90.jpg" class="img-fluid mt-5">-->
	<!--                  			<img src="./vasa_files/shutterstock_782719522.jpg" class="img-fluid scale">-->
	<!--              			</div>-->
	<!--              			<div class="d-flex flex-column">-->
	<!--              				<div class="img_size_new">-->
	<!--								<img src="./vasa_files/Vassa logo.jpg" class="">-->
	<!--							</div>-->
	              				<!-- <img src="images/12.jpg" class="img-cat"> -->
	                  <!-- <img src="images/1554116576-13511971185ca1efe0bcd5a0-26602492-500x5001.png" class="img-fluid scale mb-2"> -->
	<!--                  			<img src="./vasa_files/48_nLrmdNm.jpg.768x1024_q90.jpg" class="img-fluid main-last">-->
	<!--              			</div>-->
	<!--          			</div>-->
	<!--				</div>-->
	<!--			</div>-->
	<!--		</div>-->
	<!--	</div>-->
	<!--</section>-->
	<!--<section id="member">-->
	<!--	<div class="container">-->
	<!--		<div class="mt-5">-->
	<!--			<div class="row frist" id="six">-->
	<!--				<div class="col-md-6">-->
	<!--					<h1 class="hed-last">-->
	<!--						Member<br>Club-->
	<!--					</h1>-->
	<!--					<p class="hed-p-last">-->
	<!--						Elevate your lifestyle with Vassa's exclusive club membership. As a member, you'll gain access to a world of luxury and exclusivity, from private events and VIP experiences to personalized services and amenities that cater to your every need. Let us enhance your life's story and create unforgettable moments that you'll treasure forever. Join Vassa's exclusive member club today.-->
	<!--					</p>-->
	<!--				</div>-->
	<!--				<div class="col-md-6">-->
	<!--					<div class="shutter_img_1">-->
	<!--						<img src="./vasa_files/Vassa logo.jpg" class="img-cat">-->
	<!--					</div>-->
	<!--					<div class="shutter_img mt-2">-->
	<!--						<img src="./vasa_files/shutterstock_523240870.jpg" class="img-fluid">-->
	<!--					</div>-->
	<!--				</div>-->
	<!--			</div>-->
	<!--		</div>-->
	<!--	</div>-->
	<!--</section>-->

	<!--<section id="culinary">-->
	<!--	<div class="container">-->
	<!--		<div class="mt-5">-->
	<!--		<div class="row frist" id="seven">-->
	<!--			<div class="col-md-6 mt-5">-->
	<!--				<h1 class="f-hed">Culinary<br></h1>-->
	<!--				<img src="./vasa_files/shutterstock_141627028.jpg" class="img-fluid">-->
	<!--			</div>-->
	<!--			<div class="col-md-6">-->
	<!--				<div class="row frist">-->
	<!--				<div class="col-md-4">-->
						
	<!--				</div>-->
	<!--				<div class="col-md-4">-->
						
	<!--				</div>-->
	<!--				<div class="col-md-4 text-end col-6 p-5">-->
	<!--					<img src="./vasa_files/Vassa logo.jpg" class="img-fluid main-red">-->
	<!--				</div>-->
	<!--				<div class="pag-p">-->
	<!--					<p>-->
	<!--						Our culinary experiences are inspired by cultures from around the world, with a focus on fresh, local ingredients. We partner with renowned chefs and restaurants to offer our guests an unforgettable culinary experience. Whether it's a gourmet meal or a cooking class, our culinary experiences are tailored to meet your preferences and ensure an exceptional dining experience. Explore Vassa's culinary offerings today.-->
	<!--					</p>-->
	<!--				</div>-->
	<!--			</div>-->
	<!--			</div>-->
	<!--		</div>-->
	<!--	</div>-->
	<!--	</div>-->
	<!--</section>-->
	<!--<section id="art">-->
	<!--	<div class="container" id="Life">-->
	<!--			<div class="mt-5 mb-5">-->
	<!--		<div class="row frist" id="eight">-->
	<!--			<div class="col-md-8">-->
	<!--				<h1 class="va-min"><span class="va-hed">Art & Fashion</span> </h1>-->
	<!--			</div>-->
	<!--			<div class="col-md-4 text-end shutter_img_1">-->
	<!--				<img src="./vasa_files/Vassa logo.jpg" class="img-cat">-->
	<!--			</div>-->
	<!--		</div>-->
	<!--		<div class="row frist mt-5">-->
	<!--			<div class="col-md-6">-->
					<!-- <h1 class="va-min"><span class="va-hed" >VASSA</span>LIFE BRAND </h1> -->
					<!-- <div class="text-end">
	<!--					<img src="images/12.jpg" class="img-cat" >-->
	<!--				</div> -->-->
	<!--				<div class="image-column col-lg-8 col-md-12 col-sm-12">-->
	<!--            <div class="inner-column wow fadeInLeft">-->
	<!--              <figure class="image-1"><a class="lightbox-image" data-fancybox="images"><img src="./vasa_files/shutterstock_585678371.jpg" class="img-fluid" alt=""></a></figure>-->
	<!--              <figure class="image-2"><a class="lightbox-image" data-fancybox="images"><img src="./vasa_files/45_lpWqhfu.jpg.768x1024_q90.jpg" class="img-fluid" alt=""></a></figure>-->
	<!--              <figure class="image-3 mb-5"><a class="lightbox-image" data-fancybox="images"><img src="./vasa_files/shutterstock_1519672361.jpg" class="img-fluid" alt=""></a></figure>-->
	<!--            </div>-->
	<!--          </div>-->
	<!--			</div>-->
	<!--			<div class="col-md-6">-->
	<!--		<div class="fp-p">-->
	<!--			<p>-->
	<!--				Vassa Hospitality is not just about providing luxurious experiences but also embracing art and fashion.-->
	<!--			</p>-->
	<!--			<p>-->
	<!--				We partner with leading fashion designers, promoting exclusive collections and organizing fashion events.-->
	<!--			</p>-->
	<!--			<p>-->
	<!--				Our cultural-inspired art collections are showcased throughout our properties, providing our guests with a unique and immersive experience.-->
	<!--			</p>-->
	<!--			<p>-->
	<!--				Immerse yourself in Vassa's art and fashion offerings.-->
	<!--			</p>-->
	<!--			</div>-->
	<!--			</div>-->
	<!--		</div>-->
	<!--	</div>-->
	<!--	</div>-->
	<!--</section>-->
	<!--<section id="event" class="">-->
	<!--	<div class="container" id="nine">-->
	<!--		<div class="mt-5 mb-5">-->
	<!--			<div class="row frist" id="eight">-->
	<!--				<div class="col-md-8">-->
	<!--					<h1 class="va-min"><span class="va-hed">Events </span> </h1>-->
	<!--				</div>-->
	<!--				<div class="col-md-4 text-end shutter_img_1">-->
	<!--					<img src="./vasa_files/Vassa logo.jpg" class="img-cat">-->
	<!--				</div>-->
	<!--			</div>-->
	<!--			<div class="row frist mt-5">-->
	<!--				<div class="col-md-6">-->
 <!--   		            <div class="inner-column wow fadeInLeft">-->
 <!--   		              <figure class="image-1 padding_rt_custom"><a class="lightbox-image" data-fancybox="images"><img src="./vasa_files/Layer-2.png" class="img-fluid" alt=""></a></figure>-->
 <!--   		              <figure class="image-2 margin_custom"><a class="lightbox-image" data-fancybox="images"><img src="./vasa_files/14_success.jpg" class="img-fluid" alt=""></a></figure>-->
    		              
 <!--   		            </div>-->
	<!--				</div>-->
	<!--				<div class="col-md-6">-->
	<!--					<div class="offer_cnt">-->
	<!--						<ul>-->
	<!--							<li>Our brand philosophy is to provide exceptional and personalized experiences for our guests, and our events are no exception.</li>-->
	<!--							<li>We offer a range of food events, cultural events, and wellness events, all tailored to meet our guests' preferences.</li>-->
	<!--							<li>Our events are designed to immerse our guests in the local culture, offering an unforgettable and authentic experience.</li>-->
	<!--							<li>Discover Vassa's events today.</li>-->
								<!--<li>VASSA concierge offers menbers unparalleled access to exclusive experiences limited only by their imagination.</li>-->
	<!--						</ul>-->
							<!--<div class="signature_btm">-->
							<!--    <p>Two unrivalled brands.</p>-->
	      <!--                <em>VASSA HOTELS</em>-->
	      <!--                <em>VASSA LIFE</em>-->
							<!--</div>-->
	<!--					</div>-->
	<!--				</div>-->
	<!--			</div>-->
	<!--		</div>-->
	<!--	</div>-->
	<!--</section>-->
	
	<!--<section id="career">-->
	<!--	<div class="container">-->
	<!--		<div class="mt-5">-->
	<!--			<div class="row frist" id="six">-->
	<!--				<div class="col-md-6">-->
	<!--					<h1 class="hed-last">-->
	<!--						Career<br>-->
	<!--					</h1>-->
	<!--					<p class="hed-p-last">-->
	<!--						At Vassa Hospitality, we believe in investing in our ambassadors' growth and development, providing them with opportunities to excel and achieve their goals. We are always looking for talented individuals who share our passion for excellence and attention to detail. Join our team and become part of a brand that values and rewards dedication and hard work. Explore Vassa's career opportunities today.-->
	<!--					</p>-->
	<!--				</div>-->
	<!--				<div class="col-md-6">-->
	<!--					<div class="shutter_img_1">-->
	<!--						<img src="./vasa_files/Vassa logo.jpg" class="img-cat">-->
	<!--					</div>-->
	<!--					<div class="shutter_img mt-2">-->
	<!--						<img src="./vasa_files/shutterstock_523240870.jpg" class="img-fluid">-->
	<!--					</div>-->
	<!--				</div>-->
	<!--			</div>-->
	<!--		</div>-->
	<!--	</div>-->
	<!--</section>-->
	
<section class="brands">
  	<div class="container">
    	<div class="row frist justify-content-center">
    				<div class="col-md-4">
    					<img src="./vasa_files/Vassa logo.jpg" class="img-fluid">
    				</div>
    				<div class="col-md-3">
    					<img src="./vasa_files/brand_3.jpg" class="img-fluid">
    				</div>
			    	<!--<div class="col-md-4">-->
			    	<!--	<img src="./vasa_files/brand_2.jpg" class="img-fluid main-list img_3">-->
			    	<!--</div>-->
    	</div>
  	</div>
</section>
	
	
	<section id="partner" class=" fndmntl " style="background-image:url(https://demo.stunningcollection.com/public/theme_elevate/assets/images/FUNDAMENTALS.png);">
		<div class="container" id="ten">
			<div class="">
				<div class="row frist" id="eight">
					<div class="col-md-8">
						<h1 class="va-min"><span class="va-hed">Partners </span> </h1>
					</div>
					<!--<div class="col-md-4 text-end shutter_img_1 wht_logo">-->
					<!--	<img src="./vasa_files/heading_logo.png" class="img-cat">-->
					<!--</div>-->
				</div>
				<div class="row frist mt-5">
					<!--<div class="col-md-6">-->
    		            
					<!--</div>-->
					<div class="col-md-12">
						<div class="offer_cnt">
						   <img src="./vasa_files/EPI_Wellness_logos.png" class="img-fluid">
							<!--<ul>-->
							<!--	<li>We partner with some of the world's most prestigious brands to offer our guests the ultimate in luxury and exclusivity.</li>-->
							<!--	<li>Our partnerships include leading fashion designers, gourmet restaurants, wellness experts, and cultural institutions.</li>-->
							<!--	<li>Through these partnerships, we aim to provide our guests with exceptional experiences that enhance their lifestyles and create unforgettable memories.</li>-->
							<!--	<li>Discover Vassa's partnerships today.</li>-->
								<!--<li>Futuristic engaging entertainment for new-gen.</li>-->
								<!--<li>From winning community hearts to a going-green.</li>-->
							<!--</ul>-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<section class="chessboard-marker">
    <div class="container">
        <div class="marker-line"></div>
    </div>
</section>
<section id="comming-soon" class="comming-soon" >
	<div class="container">
	    <div class="row">
	        <h1 style="font-size:50px; text-align: center;">COMING SOON</h1>
	    </div>
	    <div class="row">
	        <div class="col-md-3"><h2 style="font-size:40px; text-align: center; padding-top: 40px; padding-bottom: 25px;">Bangladesh</h2></div>
	        <div class="col-md-3"><h2 style="font-size:40px; text-align: center; padding-top: 40px; padding-bottom: 25px;">India</h2></div>
	        <div class="col-md-3"><h2 style="font-size:40px; text-align: center; padding-top: 40px; padding-bottom: 25px;">Kenya</h2></div>
	        <div class="col-md-3"><h2 style="font-size:40px; text-align: center; padding-top: 40px; padding-bottom: 25px;">Dubai</h2></div>
	    </div>
	</div>
</section>
<section class="chessboard-marker">
    <div class="container">
        <div class="marker-line"></div>
    </div>
</section>	
	
	
	<section class="scssc_outer lets_talk_outer " style="background-image:url(https://demo.stunningcollection.com/public/theme_elevate/assets/images/lets_talk.png);">
		<div class="container" id="twelve">
			<div class="">
				<div class="row frist" id="eight">
					<div class="col-md-6">
						<div class="scssc_cnt signature_btm">
							<h3>OUR SUCCESS IS</h3>
							<em>WHEN EVERYONE WINS TOGETHER -<br>
							GUESTS, COLLEAGUES,<br>
							HOTEL OWNERS &amp; BRANDS.</em>
						</div>
					</div>
					<div class="col-md-6">
<!--						 <h1 class="va-min">Vassa hotels</h1> -->
<!--						 <div class="scssc_cnt signature_btm">-->
<!--							<p><strong>Rana Mukherji</strong> (Founder &amp; CEO)</p>-->
<!--							<p><strong></strong> <a> Global Hotelier PTE LTD</br>-->
<!--160 Robinson Road #14-04,</br> -->
<!--Singapore, 068914</a></p>-->
<!--							<p><strong>Call-</strong> <a href="tel: +971585197858"> +971 585 197 858</a></p>-->
<!--							<p><strong>Email-</strong> <a href="mailto:%info@stunningcollections.com"> info@stunningcollections.com</a></p>-->
							<!--<p><a href="https://goldbck.com/"> https://goldbck.com/</a></p>-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="contact_outer" id="contact">
		<div class="container">
			<div class="row">
				<div class="col-md-8 offset-md-2">
					<div class="heading_contact text-center">
						<h1>Contact Us</h1>
						<p>Thank you for your interest in Vassa Hospitality. For inquiries, reservations, or to learn more about our brand and offerings, please fill the contact us form below. Our dedicated team of hospitality professionals is available to assist you with any questions or requests you may have. We look forward to hearing from you and helping you elevate your life's story.</p>
					</div>
					<div class="form_outer">
						<form class="form">
							<div class="form-group" data-children-count="1">
								<input type="text" name="" placeholder="Name" class="form-control" data-kwimpalastatus="alive" data-kwimpalaid="1632114089312-0">
							</div>
							<div class="form-group" data-children-count="1">
								<input type="email" name="" placeholder="Email" class="form-control" data-kwimpalastatus="alive" data-kwimpalaid="1632114089312-1">
							</div>
							<div class="form-group" data-children-count="1">
								<input type="number" name="" placeholder="Phone Number" class="form-control" data-kwimpalastatus="alive" data-kwimpalaid="1632114089312-2">
							</div>
							<div class="form-group" data-children-count="1">
								<textarea name="" placeholder="Message" class="form-control"></textarea>
							</div>
							<button class="btn btn_submit">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="more_about_membership">
        <div class="container">
            <h2>Have more questions?</h2>
            <p>Contact your team.</p>
            <div class="travel_contact">
                <div class="left_travel_ing">
                     <img src="./vasa_files/vassa-5.jpg" class="img-fluid">
                </div>
                <div class="right_travel_contact">
                    <div class="travel_coninner">
                        <h3>Contact Us</h3>
                        <ul>    
                            <li>
                                <a href="mailto:info@stunningcollection.com;">
                                    <i class="fa fa-envelope" style="margin-right:15px;"></i> info@stunningcollection.com
                                </a>
                            </li>
                            <li>
                                <a href="https://vassatrend.com" class="add-flex">
                                    <i class="fa fa-building" style="margin-right:15px;"></i>
                                    <div class="add-grp">
                                    <p>Global Hotelier PTE LTD,</p> <p>160 Robinson Road #14-04,</p> <p>Singapore, 068914</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="tel: +971585197858"> 
                                     <i class="fa fa-phone" style="margin-right:15px;"></i> +971 585 197 858
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<section class="footer_become">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <h1>Become a Partner</h1>
                        <p>Stunning Collection has been placing discerning guests in prestigious properties. Get in touch with one of our experts.</p>
                    </div>
                    <div class="col-sm-4 d-flex align-items-center">
                        <a href="javascript:void(0);" class="submit_request" data-toggle="modal" data-target=".become-a-partner">Submit &amp; Book a Call</a>
                    </div>
                </div>
            </div>
        </section>
	
	
	<footer class="csst-footer-grp">
	    <div class="container">
	        <div class="row" style="align-items: end;">
	            <div class="col-lg-9">
	                <h1 style="font-size:55px;padding-top: 25px;">A Stunning collection brand</h1>
	                <!--<h1 class="" style="font-size:55px; padding-bottom: 10px;">Vassa hotels</h1> -->
					<div class="scssc_cnt">
    					<p><strong><a href="https://vassatrend.com/about">About founder - Elena Vassa</a></strong></p>
    				<p><a href="https://vassatrend.com"><strong>Website</strong> - www.vassatrend.com</a></p>
    					<!--<p><strong>Call-</strong> <a href="tel: +971585197858"> +971 585 197 858</a></p>-->
    					<!--<p><strong>Email-</strong> <a href="mailto:%info@stunningcollection.com"> info@stunningcollection.com</a></p>-->
	                 </div>   
	            </div>
	             
	        </div>
	        <div class="row">
	            <!--<div class="col-lg-6">-->
	            <!--    <p>Global Hotelier PTE LTD, 160 Robinson Road #14-04, Singapore, 068914 </p>-->
	            <!--</div>-->
	            <div class="col-lg-9">
	                <p>Copyright© 2023 All rights reserved. Privacy Policy</p>
	            </div>
	            <div class="col-lg-3">
	              <a href="https://instagram.com/vassa_co?igshid=YmMyMTA2M2Y=">   <img src="./vasa_files/instagram.png" class="insta-logo"></a>
	              <a href="#">   <img src="./vasa_files/facebook.png" class="insta-logo"></a>
	                  
	             </div>
	            
	        </div>
	   </div>
	</footer>
	
	
	
	<div id="become_amember" class="modal fade become-a-partner" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <button type="button" class="close model_close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                <div class="modal-body">
                    <div class="wrap_tabsec">
                        <div class="coomn_form">
                          <h2 style="color: #000;">Property Owner's Application Form</h2>
                          <form method="POST" action="" autocomplete="off">
                            <input type="hidden" name="_token" value="">
                            <div class="form-group">
                                <input type="text" name="first_name" class="form-control" required="" placeholder="First Name" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" name="last_name" class="form-control" required="" placeholder="Last Name" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" required="" placeholder="Email" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" class="form-control" required="" placeholder="Phone Number" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" name="country" class="form-control" required="" placeholder="Location(Country)" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" name="location" class="form-control" required="" placeholder="Location(City/Town/Region)" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" name="link" class="form-control" required="" placeholder="Website or link to property photos" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label class="custom_checkbox"> I’d like to sign up for the Stunning Collection newsletter
                                  <input type="checkbox" name="newsletter">
                                  <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Submit Request" class="page_btn">
                            </div>
                          </form> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<script src="./vasa_files/jquery-3.5.1.slim.min.js.download" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="./vasa_files/popper.min.js.download" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="./vasa_files/bootstrap.min.js.download" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script>
     $(document).ready(function() {
    $('#download_pdf').click(function(e) {
      e.preventDefault();  // prevent the link from navigating to the PDF file directly
      var url = $(this).attr('href');
      window.location.href = url;
    });
  });
    
        $(document).on("scroll", function () {
            if ($(document).scrollTop() > 80) {
                $(".header_outer").addClass("shrink");
            } else {
                $(".header_outer").removeClass("shrink");
            };
        });
        $(document).ready(function(){
          $(".toggle_btn").click(function(){
            $(".menu_items > ul").toggleClass("open")
          });
        });
    </script>
    <script>
    
    var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 5px solid #fff}";
        document.body.appendChild(css);
    };
</script>

</body><grammarly-desktop-integration data-grammarly-shadow-root="true"></grammarly-desktop-integration><loom-container id="lo-engage-ext-container"><loom-shadow classname="resolved"></loom-shadow></loom-container></html>